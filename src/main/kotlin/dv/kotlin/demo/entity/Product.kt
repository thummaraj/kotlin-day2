package dv.kotlin.demo.entity

data class Product (var name: String,
                    var description: String,
                    var price:Double,
                    var quantity: Int) {
}