package dv.kotlin.demo.controller

import dv.kotlin.demo.entity.Product
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class AppController {
    @GetMapping("/getAppName")
    fun getAppName():String {
        return "assessment"
    }
    @GetMapping("/product")
    fun getProduct(): ResponseEntity<Any>{
        val product = Product("iPhone","A new telephone",28000.0,5)
        return ResponseEntity.ok(product)
    }
    @GetMapping("/product/{name}")
    fun getProductPath(@PathVariable("name") name:String) : ResponseEntity<Any>{
        val product = Product("iPhone","A new telephone",28000.0,5)
        if(product.name.equals(name)){
            return ResponseEntity.ok(product)
        } else{
            return ResponseEntity.notFound().build()
        }

    }
    @PostMapping("/setZeroQuantity")
    fun getSetZero(@RequestBody product: Product): ResponseEntity<Any>{
        return ResponseEntity.ok(product)
    }
    @PostMapping("/totalPrice")
    fun getTotalPrice(@RequestBody products: List<Product>): ResponseEntity<Any>{
        var total: Double = 0.0
        for(i in products.indices){
            total = total + products[i].price
        }
        return ResponseEntity.ok("$total")
    }
    @PostMapping("/avaliableProduct")
    fun getAvaliable(@RequestBody products: List<Product>): ResponseEntity<Any>{
        var output = mutableListOf<Product>()
        for(i in products.indices){
            if (products[i].quantity !== 0){
                output.add(products[i])
            }
        }
        return ResponseEntity.ok(output)
    }
}