package dv.kotlin.demo.controller

import dv.kotlin.demo.entity.Person
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.*

@RestController
class HelloWorldController{
    @GetMapping("/helloWorld")
    fun getHelloWorld(): String {
        return "helloWorld"
    }
    @GetMapping("/person")
    fun getPerson(): ResponseEntity<Any>{
        val person = Person("somchai","somrak",15)
        return ResponseEntity.ok(person)
    }
    @GetMapping("/myPerson")
    fun getmyPerson(): ResponseEntity<Any>{
        val person = Person("Krittayanee","Thummaraj",21)
        return ResponseEntity.ok(person)
    }
    @GetMapping("/myPersons")
    fun getmyPersons(): ResponseEntity<Any>{
        val person = Person("Krittayanee","Thummaraj",21)
        val person2 = Person("Titi","Supisara",21)
        val person3 = Person("Pee","Nut",21)
        val persons = listOf<Person>(person,person2,person3)
        return ResponseEntity.ok(persons)
    }
   @GetMapping("/params")
   fun getParams(@RequestParam("name") name:String,@RequestParam("surname") surname:String): ResponseEntity<Any>{
       return ResponseEntity.ok("$name $surname")
   }
    @GetMapping("/params/{name}/{surname}/{age}")
    fun getPathParam(@PathVariable("name") name: String,
                     @PathVariable("surname") surname: String,
                     @PathVariable("age") age:Int): ResponseEntity<Any>{
        val person = Person(name,surname, age)
        return ResponseEntity.ok(person)
    }
    @PostMapping("/echo")
    fun echo(@RequestBody person: Person): ResponseEntity<Any>{
        return ResponseEntity.ok(person)
    }
}